import React from 'react';

import './components/_settings/App.css';
import {Pomodoro} from "./components/templates/Pomodoro";

function App() {



  return (
    <div className="App">
      <Pomodoro></Pomodoro>
    </div>
  );
}

export default App;
