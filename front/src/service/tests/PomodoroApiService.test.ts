import * as Service from "../PomodoroApiService"
import {act} from "react";
import {IHistory} from "../PomodoroApiService";

let spyFetch : jest.SpyInstance

beforeEach(()=>{
    spyFetch = jest.spyOn(global,"fetch").mockResolvedValueOnce({} as any)
})

test("PomodoroApiService GET test", async () => {
    await act(async ()=>{
        await Service.GET()
    })

    expect(spyFetch).toHaveBeenCalled()
})


test("PomodoroApiService POST test", async () => {
    const body: IHistory = {
        pomodoro_name: "pomodoro",
        time: 1000,
    }

    await act(async ()=>{
        await Service.POST(body)
    })

    expect(spyFetch).toHaveBeenCalled()
})


test("PomodoroApiService PUT test", async () => {
    const body: IHistory = {
        pomodoro_name: "pomodoro",
        time: 1000,
    }

    await act(async ()=>{
        await Service.PUT(1,body)
    })

    expect(spyFetch).toHaveBeenCalled()
})
