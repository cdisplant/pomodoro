const baseUrl = "http://localhost:3002/pomodoro/";

const baseOption: RequestInit = {
    headers:{
        "Accept": "application/json",
        "Content-Type": "application/json",
    }
}

export type IHistory = {
    id?: number
    pomodoro_name: string
    time: number
}

export function GET(){
    const options: RequestInit = baseOption
    options.method = "GET"
    return fetch(baseUrl, options)
}

export function POST(body: IHistory){
    const options: RequestInit = baseOption
    options.method = "POST"
    options.body = JSON.stringify(body)
    return fetch(baseUrl, options)
}
export function PUT(id: number,body: IHistory){
    const options: RequestInit = baseOption
    options.method = "PUT"
    return fetch(baseUrl+id, options)
}
