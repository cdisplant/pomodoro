import App from "./App";
import {render, screen} from "@testing-library/react";


test("App render",()=>{
    render(<App></App>)

    expect(screen.getByText("Pomodoro")).toBeInTheDocument()
})
