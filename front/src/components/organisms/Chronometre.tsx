import {useState} from "react";
import {Container} from "../atoms/Container";
import {Timer} from "../molecules/Timer";
import {Button} from "../atoms/Button";

interface Props {
    start : {
        minutes: number;
        seconds: number;
    }
    onEnd?: () => void;
}

export function Chronometre({start, onEnd}: Props){

    const [paused, setPaused] = useState(true);

    return <Container>
        <Timer start={start} paused={paused} OnEnd={onEnd}></Timer>
        <Button onClick={()=>{setPaused(!paused)}}>{paused?"Reprendre":"Pause"}</Button>
    </Container>
}
