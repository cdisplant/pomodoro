import React from 'react';
import { render, screen } from '@testing-library/react';
import {History} from "../History";
import {IHistory} from "../../../service/PomodoroApiService";


test('render history correcly', () => {
    const sessions : IHistory[] = [
        {
            time: 2400,
            pomodoro_name: "test_pomodoro1",
            id: 1
        },
        {
            time: 2400,
            pomodoro_name: "test_pomodoro2",
            id: 2
        }
    ]

    render(<History sessions={sessions} />);

    expect(screen.getByText(sessions[0].pomodoro_name)).toBeInTheDocument()
    expect(screen.getByText(sessions[1].pomodoro_name)).toBeInTheDocument()

})
