import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import {Chronometre} from "../Chronometre";

test('render Chronometre correctly', async () => {
    const start = {
        minutes:40,
        seconds:30
    }

    const OnEnd = jest.fn()

    render(<Chronometre start={start} onEnd={OnEnd}/>);

    const button = screen.getByText("Reprendre")

    expect(button).toBeInTheDocument()
    const label = button.innerText
    fireEvent.click(button)

    expect(screen.getByText(start.minutes.toString())).toBeInTheDocument()
    expect(screen.getByText(start.seconds.toString())).toBeInTheDocument()

    expect(button).not.toHaveTextContent(label)
    await new Promise(resolve => setTimeout(resolve, 2000))

    expect(screen.queryByText(start.seconds.toString())).toBeNull()
})
