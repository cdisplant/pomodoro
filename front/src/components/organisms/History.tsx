import {IHistory} from "../../service/PomodoroApiService";
import {Session} from "../molecules/Session";

interface Props {
    sessions: IHistory[]
}

export function History({ sessions }: Props) {
    return <div style={{display: "flex", flexDirection: "column", justifyContent: "space-between", gap: 15}}>
        <h2>Historique</h2>
        {sessions.map((session: IHistory) => <Session session={session} key={session.id} />)}
    </div>
}
