import {Container} from "../atoms/Container";
import {Button} from "../atoms/Button";
import {Chronometre} from "../organisms/Chronometre";
import {useEffect, useState} from "react";
import * as Services from "../../service/PomodoroApiService"
import {History} from "../organisms/History";
import {IHistory} from "../../service/PomodoroApiService";



export function Pomodoro(){

    const [time, setTime] = useState({minutes: 25, seconds: 0});
    const [isPause, setIsPause] = useState(false);
    const [idSession, setIdSession] = useState(undefined);
    const [sessions, setSessions] = useState<IHistory[]>([]);

    function onEnd(){
        if(isPause){
            setIsPause(false);
            setTime({minutes: 25, seconds: 0});
        }else {
            setIsPause(true);
            setTime({minutes: 5, seconds: 0});
            if(!idSession) {
                Services.POST({
                    pomodoro_name:"",
                    time:25
                }).then(r=>r.json()).then(r=>setIdSession(r.id));
            }else{
                Services.GET().then(r=>r.json())
                    .then((r:Services.IHistory[])=>r.find(h=>h.id===idSession))
                    .then(h=>{
                        if (h){
                            h.time += 30
                            Services.PUT(idSession,h)
                        }
                    })
            }

        }
    }

    useEffect(()=>{
        let ignore = false;
        setSessions([]);
        Services.GET().then(r=>r.json()).then(r=>{
            if(!ignore){
                setSessions(r)
            }
        })
        return ()=>{
            ignore = true;
        }
    },[idSession])

    return <Container>
        <Container style={{display: "flex", flexDirection: "row" , gap:"10px"}}>
            <Button onClick={()=>{
                setIsPause(false);
                setTime({minutes: 25, seconds: 0});
            }}>Pomodoro</Button>
            <Button onClick={()=>{
                setIsPause(true)
                setTime({minutes: 15, seconds: 0});
            }}>Grosse Pause</Button>
            <Button onClick={()=>{
                setIsPause(true)
                setTime({minutes: 5, seconds: 0});
            }}>Petite Pause</Button>
        </Container>
        <Chronometre start={time} key={Math.random()} onEnd={onEnd}></Chronometre>
        <History sessions={sessions}></History>
    </Container>
}
