import * as Services from "../../../service/PomodoroApiService";
import {fireEvent, render, screen} from "@testing-library/react";
import {Pomodoro} from "../Pomodoro";
import React from "react";

    jest.mock("../../organisms/Chronometre", () => ({
        Chronometre: ({onEnd,start}:{
            onEnd:()=>void,
            start : {
                minutes: number;
                seconds: number;
            }})=>{
            console.log("test_pomodoro1");

            return <><button onClick={onEnd}>ButtonOnEnd</button><span>{start.minutes}:{start.seconds}</span></>
        }
    }))

test('OnEnd works', async () => {
    const spyPost = jest.spyOn(Services,"POST").mockResolvedValue({json: jest.fn().mockResolvedValue({})} as any)

    render(<Pomodoro />);

    expect(screen.getByText('25:0')).toBeInTheDocument();
    fireEvent.click(screen.getByText('ButtonOnEnd'))
    expect(screen.getByText('5:0')).toBeInTheDocument();
    expect(spyPost).toBeCalled()


})

