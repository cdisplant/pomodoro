import React, {act} from 'react';
import {findByText, fireEvent, render, screen, waitFor} from '@testing-library/react';
import {Pomodoro} from "../Pomodoro";
import * as Services from "../../../service/PomodoroApiService"
import {IHistory} from "../../../service/PomodoroApiService";
import {Chronometre} from "../../organisms/Chronometre";
import mock = jest.mock;

let spy: jest.SpyInstance;
beforeEach(()=>{
    const history : IHistory[] = [
        {
            time: 2400,
            pomodoro_name: "test_pomodoro1",
            id: 1
        },
        {
            time: 2400,
            pomodoro_name: "test_pomodoro2",
            id: 2
        }
    ]

    spy = jest.spyOn(Services,"GET").mockResolvedValue({
        json: jest.fn().mockResolvedValue(history)
    } as any)


})

afterEach(() => {
    jest.restoreAllMocks();
});


test('renders pomodoro correctly', async () => {

    render(<Pomodoro />);

    await act(async ()=> await waitFor(()=>expect(spy).toHaveBeenCalled()))

    expect(screen.getByText('Historique')).toBeInTheDocument();
    expect(screen.getByText("test_pomodoro1")).toBeInTheDocument()

    const pomodoro_button = screen.getByText('Pomodoro')
    const tinypause_button = screen.getByText('Petite Pause')
    const bigpause_button = screen.getByText('Grosse Pause')

    expect(pomodoro_button).toBeInTheDocument();
    expect(tinypause_button).toBeInTheDocument();
    expect(bigpause_button).toBeInTheDocument();
})

test('button pomodoro works', async () => {
    render(<Pomodoro />);

    const pomodoro_button = screen.getByText('Pomodoro')

    expect(pomodoro_button).toBeInTheDocument();
    fireEvent.click(pomodoro_button);

    expect(screen.getByText("25"))
})

test('button tiny works', async () => {
    render(<Pomodoro />);

    const tinypause_button = screen.getByText('Petite Pause')

    expect(tinypause_button).toBeInTheDocument();
    fireEvent.click(tinypause_button);

    expect(screen.getByText("5"))
})

test('button big works', async () => {
    render(<Pomodoro />);

    const bigpause_button = screen.getByText('Grosse Pause')

    expect(bigpause_button).toBeInTheDocument();
    fireEvent.click(bigpause_button);

    expect(screen.getByText("15"))
})
