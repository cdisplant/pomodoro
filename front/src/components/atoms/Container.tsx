import {CSSProperties, PropsWithChildren} from "react";

interface Props extends PropsWithChildren {
    style?: CSSProperties;
}

export function Container({children, style = {}}:Props) {
    return <div style={style}>{children}</div>
}
