import "./styles/Clock.scss"

interface Props {
    minutes: number;
    seconds: number;
}

export function Clock({minutes,seconds}:Props){
    return <div>
        <span className="minutes">{minutes}</span>
        <span className="tick">:</span>
        <span className="seconds">{seconds}</span>
    </div>
}
