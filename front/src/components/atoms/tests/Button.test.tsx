import React from 'react';
import { render, screen } from '@testing-library/react';
import { fireEvent } from '@testing-library/react';
import {Button} from "../Button";

test('render button correcly', () => {
    const testlabel = "bouton test"
    const testfn = jest.fn()
    render(<Button onClick={testfn}>{testlabel}</Button>)

    const button = screen.getByText(testlabel)

    expect(button).toBeInTheDocument()
    expect(button).toHaveTextContent(testlabel)

    fireEvent.click(button)

    expect(testfn).toHaveBeenCalled()
})
