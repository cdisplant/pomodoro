import React from 'react';
import { render, screen } from '@testing-library/react';
import {Clock} from "../Clock";
import {text} from "node:stream/consumers";

test('render container correctly',()=>{
    const min= 12
    const sec= 34
    const testid = "test"
    render(<Clock data-testid={testid} seconds={sec} minutes={min}></Clock>)

    expect(screen.getByText(min.toString())).toBeInTheDocument()
    expect(screen.getByText(sec.toString())).toBeInTheDocument()
})
