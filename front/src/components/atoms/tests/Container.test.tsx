import React from 'react';
import { render, screen } from '@testing-library/react';
import {Container} from "../Container";

test('render container correctly', async () => {
    const text = "test"
    render(<Container>{text}</Container>);

    const container = screen.getByText(text)

    expect(container).toBeInTheDocument()
    expect(container).toHaveTextContent(text)
})
