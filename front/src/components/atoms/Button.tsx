import {PropsWithChildren} from "react";
import './styles/Button.scss'

interface Props extends PropsWithChildren{
    onClick?: () => void;
}

export function Button({children,onClick}:Props){
    return <button onClick={onClick}>{children}</button>
}
