import React from 'react';
import { render, screen } from '@testing-library/react';
import {IHistory} from "../../../service/PomodoroApiService";
import {Session} from "../Session";

test('render session correcly', ()=>{
    const session: IHistory = {
        time: 2400,
        pomodoro_name: "test_pomodoro",
        id: 1
    }

    render(<Session session={session} />)
    expect(screen.getByText(session.pomodoro_name)).toBeInTheDocument()
    expect(screen.getByText(/dur.e: \d+/)).toBeInTheDocument()
})
