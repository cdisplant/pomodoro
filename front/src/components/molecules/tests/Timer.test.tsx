import React, {act} from 'react';
import { render, screen } from '@testing-library/react';
import {Timer} from "../Timer";

test('render timer correcly', ()=>{
    const start = {
        minutes:40,
        seconds:30
    }
    let paused = true

    const OnEnd = jest.fn()

    render(<Timer start={start} paused={paused} OnEnd={OnEnd}/>);

    expect(screen.getByText(start.minutes.toString())).toBeInTheDocument()
    expect(screen.getByText(start.seconds.toString())).toBeInTheDocument()
    expect(OnEnd).not.toHaveBeenCalled()
})

jest.useFakeTimers()
test('timer time pass', async ()=>{
    const start = {
        minutes:0,
        seconds:2
    }
    let paused = false
    const OnEnd = jest.fn()

    render(<Timer start={start} paused={paused} OnEnd={OnEnd}/>);

    await act(()=>jest.advanceTimersByTime(2000))
    expect(screen.queryByText('59')).toBeNull()
    await act(()=>jest.advanceTimersByTime(2000))
    expect(screen.queryByText('59')).toBeNull()
    await act(()=>jest.advanceTimersByTime(2000))

    expect(screen.queryByText(start.seconds.toString())).toBeNull()
    expect(OnEnd).toHaveBeenCalled()

})
