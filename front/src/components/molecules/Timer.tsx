import {useEffect, useState} from "react";
import {Clock} from "../atoms/Clock";

interface Props {
    readonly start: {
        minutes: number;
        seconds: number;
    }
    paused: boolean

    readonly OnEnd?: () => void
}

export function Timer({start, paused, OnEnd}: Props){

    const [min,setMin]= useState(start.minutes)
    const [sec,setSec] = useState(start.seconds)


    function tick(){
        let tempMin = min
        let tempSec = sec - 1

        if(tempSec < 0){
            tempMin = min - 1
            if(tempMin > 0){
                tempSec = 59
            }
            if(tempMin <= 0 && OnEnd){
                OnEnd()
            }
        }

        setMin(tempMin)
        setSec(tempSec)
    }

    useEffect(()=>{
        const interval = setInterval(()=>{
            if(!paused){
                tick()
            }
        },1000)

        return () => clearInterval(interval)
    })

    return <div>
        <Clock minutes={min} seconds={sec}></Clock>
    </div>
}
