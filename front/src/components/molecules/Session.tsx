import {IHistory} from "../../service/PomodoroApiService";
import {Container} from "../atoms/Container";

interface Props {
    readonly session: IHistory
}

export function Session({session}: Props) {
    return <Container>
        <h3>{session.pomodoro_name}</h3>
        <span>durée: {(session.time-(session.time%30))/30} itérations</span>
    </Container>
}
