const { Router } = require('express')
const service = require('./controller')
const fs = require("fs");
const YAML = require("yaml");
const swaggerUI = require("swagger-ui-express");

const router = Router()

router.get('/', service.getAllPorodomo)
router.post('/', service.createPorodomo)
router.put('/:id', service.updatePorodomo)

const file = fs.readFileSync('./openapi.yaml', 'utf8');
const swaggerDocument = YAML.parse(file);

router.use('/doc', swaggerUI.serve);
router.get('/doc', swaggerUI.setup(swaggerDocument));

module.exports = router
