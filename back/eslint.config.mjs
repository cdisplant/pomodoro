import js from "@eslint/js";

export default [
    js.configs.recommended,

    {
        languageOptions: {
            ecmaVersion: 12,
            sourceType: 'script',
            globals: {
                require: 'readonly',
                module: 'readonly',
                exports: 'readonly',
                __dirname: 'readonly',
                __filename: 'readonly'
            }
        },
        rules: {
            "no-unused-vars": "warn",
            "no-undef": "warn"
        }
    }
];