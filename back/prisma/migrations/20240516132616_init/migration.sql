-- CreateTable
CREATE TABLE "History" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "pomodoro_name" TEXT NOT NULL,
    "time" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "History_pomodoro_name_key" ON "History"("pomodoro_name");
