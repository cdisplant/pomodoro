const prisma = require('./client');

const getAllPorodomo = async (req, res) => {
    try{
        const pomodoros = await prisma.history.findMany()
        res.status(200).json(pomodoros)
    } catch (error){
        res.status(500).send({"error": error.message})
    }

}

const createPorodomo = async (req, res) => {
    try{
        const {pomodoro_name, time} = req.body

        await prisma.history.create({
            data: {pomodoro_name, time},
        })
        res.status(201).send("Pomodoro created with success")
    } catch (error){
        res.status(500).send({"error": error.message})
    }
}

const updatePorodomo = async (req, res) => {
    try {
        const {pomodoro_name, time} = req.body
        const id  = parseInt(req.params.id)


        await prisma.history.update({
            where: { id },
            data: {
                pomodoro_name,
                time
            },
        })
        res.status(200).send("Pomodoro updated with success")
    } catch (error){
        res.status(500).send({"error": error.message})
    }
}

module.exports = {getAllPorodomo, createPorodomo, updatePorodomo}