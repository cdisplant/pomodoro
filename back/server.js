const express = require('express')
const cors = require('cors')
const pomodoroRouter = require('./route')


const app = express()
const port = process.env.SERVICE_PORT || 3002;

app.use(express.json())
app.use(cors())

app.get("/", (req, res) => {
    res.send("Hello world")
})

app.use('/pomodoro', pomodoroRouter)

app.listen(port, () => console.log(`Server is listening on port ${port}`))

module.exports = app;