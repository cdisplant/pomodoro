import {use} from 'chai';
import chaiHttp from 'chai-http';
import sinon from 'sinon';
import prisma from '../client.js';
import {createPorodomo, getAllPorodomo, updatePorodomo} from '../controller.js';

const chai = use(chaiHttp);


describe('Pomodoro Controller', () => {
    let req, res, prismaMock;

    beforeEach(() => {
        req = {
            body: {},
            params: {}
        };
        res = {
            status: sinon.stub().callsFake(function(status) {
                this.statusCode = status;
                return this;
            }),
            json: sinon.stub(),
            send: sinon.stub()
        };
        prismaMock = prisma;

    });

    afterEach(() => {
        sinon.restore();
    });

    describe('getAllPorodomo', () => {
        it('should return all pomodoros with status 200', async () => {
            prismaMock.history.findMany = sinon.stub().resolves();

            await getAllPorodomo(req, res);

            chai.expect(res.status.calledWith(200)).to.be.true;
        });
    });

    describe('getAllPorodomo Error', () => {
        it('should not return all pomodoros with status 500', async () => {
            prismaMock.history.findMany = sinon.stub().rejects(new Error('Internal Server Error'));

            await getAllPorodomo(req, res);

            chai.expect(res.status.calledWith(500)).to.be.true;

            // findManyStub.restore();
        });
    });

    describe('createPorodomo', () => {
        it('should create Porodomo pomodoro with status 200', async () => {

            req.body = { pomodoro_name: 'Create Pomodoro', time: 30 };
            prismaMock.history.create = sinon.stub().resolves(req.body);

            await createPorodomo(req, res);

            chai.expect(res.status.calledWith(201)).to.be.true;
            chai.expect(res.send.calledWith('Pomodoro created with success')).to.be.true;
        });
    });

    describe('createPorodomo Error', () => {
        it('should not create Porodomo pomodoro with status 500', async () => {
            req.body = { pomodoro_name: 'Create Pomodoro', time: 30 };

            prismaMock.history.create = sinon.stub().rejects(new Error('Internal Server Error'));

            await createPorodomo(req, res);

            chai.expect(res.status.calledWith(500)).to.be.true;
        });
    });

    describe('updatePorodomo', () => {
        it('should update a pomodoro and return status 200', async () => {
            req.params.id = 1;
            req.body = { pomodoro_name: 'Updated Pomodoro', time: 30 };
            prismaMock.history.update = sinon.stub().resolves(req.body);

            await updatePorodomo(req, res);

            chai.expect(res.status.calledWith(200)).to.be.true;
            chai.expect(res.send.calledWith('Pomodoro updated with success')).to.be.true;
        });
    });
    describe('updatePorodomo with error', () => {
        it('should not update a pomodoro and return status 500', async () => {
            req.body = { pomodoro_name: 'Updated Pomodoro', time: 30 };
            prismaMock.history.update = sinon.stub().rejects(new Error("Internal Server Error"));

            await updatePorodomo(req, res);

            chai.expect(res.status.calledWith(500)).to.be.true;
        });
    });
});
