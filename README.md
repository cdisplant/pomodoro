# Pomodoro

## Description
Ce projet consiste à créer une application Pomodoro utilisant un mono repo avec un frontend React, un backend Express.js et l'ORM Prisma connecté à une base de données SQLite.

## Architecture
L'architecture du projet est la suivante :

- Frontend React : Le frontend React sera responsable de l'interface utilisateur de l'application, y compris l'affichage du timer, des boutons de contrôle et des statistiques.
- Backend Express.js : Le backend Express.js sera responsable de l'API RESTful de l'application, qui permettra au frontend de récupérer et de stocker les données de pomodoro.
- ORM Prisma : Prisma sera utilisé comme ORM pour mapper les modèles de données de l'application à la base de données SQLite.
- Autres Technologies :
  - Frontend : SCSS
  - Backend : Cors, Dotenv
  - Base de données: SQLite

## Installation
Pour installer le projet, suivez les étapes suivantes :
1. Clonner le repo
    ```bash
    $ git clone https://gitlab.com/cdisplant/pomodoro.git
    ```
2. Installer le front
    ```bash
    $ cd front
    $ npm i 
    ```
3. Installer le Back
    ```bash
    $ cd back
    $ yarn install
    ```
    Installer la Base de données
    ```bash
    $ yarn prisma db push
    ```
4. Lancer les différentes couche applicative
    - pour le back
   ```bash
   $ yarn start
    ```
   - pour le front
   ```bash
   $ npm start
   ```
Vous pourrez retrouver l'application à l'adresse http://localhost:3000
